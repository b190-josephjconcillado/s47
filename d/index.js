// USING DOM
// before, we are using the following codes in terms of selecting elements inside the html file
// document refers to the whole webpage and the "getElementById", "getElementByClassName",

// document.getElementById('txt-first-name');
// document.getElementsByClassName('txt-last-name');
// document.getElementsByTagName('input');

/**
 * the function used is the addEventListener - allows a block of codes to listen to an event for them to be executed
 *      addEventListener - takes two arguments: 
 *          1 - a string that identifies the event to which the codes will listen;
 *          2 - a function that the listener will execute once the specified event is triggered.
 *      .innerHTML - this allows the element to record/duplicate the value of the selected variable 
 *      .value - is needed since without it the .innerHTML will only record what type of element the target variable is inside the HTML document instead of getting its value
 */

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup',(event) => {
    spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup',(event) => {
    console.log(event.target);
    console.log(event.target.value);
});
